package com.rani.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.rani.model.Lancamento;

public class Lancamentos implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private EntityManager manager;
	
	//o construtor recebe um entity manager
	public Lancamentos(EntityManager manager){
		this.manager = manager;
	}
	
	//consulta os lancamentos e retorna um list
	public List<Lancamento> todos() {
		TypedQuery<Lancamento> query = manager.createQuery("from Lancamento", Lancamento.class);
		return query.getResultList();
	}
	
	public void adicionar(Lancamento lancamento) {
		this.manager.persist(lancamento);
	}
	
}
