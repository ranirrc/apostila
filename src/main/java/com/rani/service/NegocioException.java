package com.rani.service;

public class NegocioException extends Exception {

	private static final long serialVersionIUD = 1L;
	
	public NegocioException(String msg){
		super(msg);
	}
}
