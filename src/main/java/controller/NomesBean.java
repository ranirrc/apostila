package controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;

@ManagedBean
@ViewScoped
public class NomesBean {

	private String nome;
	private List<String> nomes = new ArrayList<>();
	private boolean termoAceito;
	private Integer[] assuntosInteresse;

	private HtmlInputText inputNome;
	private HtmlCommandButton botaoAdicionar;

	public void adicionar() {
		this.nomes.add(nome);

		// desativa o campo e botão quando mais q 3 nomes forem adicionados
		if (this.nomes.size() > 3) {
			this.inputNome.setDisabled(true);
			this.botaoAdicionar.setDisabled(true);
			this.botaoAdicionar.setValue("Muitos nomes adicionados...");
		}

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public HtmlInputText getInputNome() {
		return inputNome;
	}

	public void setInputNome(HtmlInputText inputNome) {
		this.inputNome = inputNome;
	}

	public HtmlCommandButton getBotaoAdicionar() {
		return botaoAdicionar;
	}

	public void setBotaoAdicionar(HtmlCommandButton botaoAdicionar) {
		this.botaoAdicionar = botaoAdicionar;
	}

	public List<String> getNomes() {
		return nomes;
	}

	public boolean isTermoAceito() {
		return termoAceito;
	}

	public void setTermoAceito(boolean termoAceito) {
		this.termoAceito = termoAceito;
	}

	//array de int pq pode selecionar mais de um na tela
	public Integer[] getAssuntosInteresse() {
		return assuntosInteresse;
	}

	//array de int pq pode selecionar mais de um na tela
	public void setAssuntosInteresse(Integer[] assuntosInteresse) {
		this.assuntosInteresse = assuntosInteresse;
	}
}
