package controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import com.rani.model.Lancamento;
import com.rani.repository.Lancamentos;
import com.rani.util.JpaUtil;

@ManagedBean
@ViewScoped
public class ConsultaLancamentosBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<Lancamento> lancamentos;
	
	public void consultar(){
		//instancia a classe q intermedia com o banco
		EntityManager manager = JpaUtil.getEntityManager();
		
		Lancamentos lancamentos = new Lancamentos(manager);
		
		this.lancamentos = lancamentos.todos();
		
		manager.close();
	}
	
	public List<Lancamento> getLancamentos(){
		return lancamentos;
	}
}
